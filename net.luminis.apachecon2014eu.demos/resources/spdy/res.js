// A tiny JavaScript...
function test() {
	var con;

	if (window.chrome) {
		var lt = window.chrome.loadTimes();
		con = '<p>I was requested using a <b>' + lt.connectionInfo + '</b> connection, and ';
	} else {
		con = '<p>Not sure how I was requested, but ';
	}

	if (performance && performance.timing) {
		var p = performance.timing;
		con += 'it took ' + (p.loadEventEnd - p.navigationStart) + ' ms.</p>';
	}

	var link = window.location.toString();
	if (link.indexOf('https') == 0) {
		link = link.replace(/^https:/, 'http:').replace(':8443', ':8080');
		text = 'plain';
	} else {
		link = link.replace(/^http:/, 'https:').replace(':8080', ':8443');
		text = 'spdy';
	}

	con += '<p><a href="' + link + '">Take the ' + text + ' route...</a></p>';
	con += '<p class="small">You might need to disable caching to get correct result...</p>';

	var el = document.getElementById('out');
	el.innerHTML = con; 
}
