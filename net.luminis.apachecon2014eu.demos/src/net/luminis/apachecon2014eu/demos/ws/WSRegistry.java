package net.luminis.apachecon2014eu.demos.ws;

import static net.luminis.apachecon2014eu.demos.ws.Constants.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WSRegistry {
	private final ConcurrentMap<WSClient, Boolean> m_registry = new ConcurrentHashMap<>();
	private final Set<WSClient> m_clients = Collections.newSetFromMap(m_registry);

	public boolean add(WSClient client) {
		if (m_clients.add(client)) {
			broadcastUserConnected(client);
			
			sendMsg(client, USER_ID);
			sendMsg(client, USER_LIST, getUserList());

			return true;
		}
		return false;
	}

	public void broadcastCommand(WSClient sender, String commands) {
		for (WSClient client : m_clients) {
			if (client != sender) {
				sendMsg(client, sender, COMMAND, commands);
			}
		}
	}

	public void disconnectAll() {
		for (WSClient client : m_clients) {
			client.disconnect();
		}
	}

	public int getClientCount() {
		return m_clients.size();
	}

	public boolean remove(WSClient client) {
		if (m_clients.remove(client)) {
			broadcastUserDisconnected(client);
			return true;
		}
		return false;
	}

	private void broadcastUserConnected(WSClient newUser) {
		for (WSClient client : m_clients) {
			if (client != newUser) {
				sendMsg(client, newUser, USER_CONNECTED, client.getLevel(), client.getNickName());
			}
		}
	}

	private void broadcastUserDisconnected(WSClient newUser) {
		for (WSClient client : m_clients) {
			if (client != newUser) {
				sendMsg(client, newUser, USER_DISCONNECTED, client.getLevel(), client.getNickName());
			}
		}
	}

	private String createMsg(WSClient client, Object... commands) {
		StringBuilder sb = new StringBuilder();
		sb.append(client.getId());
		for (Object command : commands) {
			sb.append(",");
			sb.append(command == null ? "" : command);
		}
		return sb.toString();
	}

	private String getUserList() {
		StringBuilder sb = new StringBuilder();
		for (WSClient client : m_clients) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			sb.append(client.getId());
			sb.append(",");
			sb.append(client.getLevel());
			sb.append(",");
			sb.append(client.getNickName() == null ? "" : client.getNickName());
		}
		return sb.toString();
	}

	private void sendMsg(WSClient recipient, Object... commands) {
		sendMsg(recipient, recipient, commands);
	}

	private void sendMsg(WSClient recipient, WSClient sender, Object... commands) {
		try {
			recipient.sendMessage(createMsg(sender, commands));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}