package net.luminis.apachecon2014eu.demos.ws;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator
{
    public void start(BundleContext context) throws Exception
    {
		Dictionary<String, Object> props = new Hashtable<>();
		props.put("alias", "/etchasketch");

		context.registerService(Servlet.class.getName(), new WebSocketDemo(), props);
    }

    public void stop(BundleContext context) throws Exception
    {
    	// Nop
    }
}
