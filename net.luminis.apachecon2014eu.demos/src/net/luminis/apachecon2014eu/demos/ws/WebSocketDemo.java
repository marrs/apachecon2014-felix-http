package net.luminis.apachecon2014eu.demos.ws;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

public class WebSocketDemo extends WebSocketServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PROTOCOL = "etchasketch";

	private WSRegistry m_registry;

	@Override
	public void destroy() {
		try {
			if (m_registry != null) {
				m_registry.disconnectAll();
				m_registry = null;
			}
		} finally {
			super.destroy();
		}
	}

	public WebSocket doWebSocketConnect(HttpServletRequest request, String protocol) {
		if (PROTOCOL.equals(protocol)) {
			return new WSClient(m_registry);
		}
		return null;
	}

	@Override
	public void init() throws ServletException {
		super.init();

		m_registry = new WSRegistry();
	}
}
