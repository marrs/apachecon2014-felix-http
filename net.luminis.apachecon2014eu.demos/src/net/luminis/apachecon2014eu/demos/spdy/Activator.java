package net.luminis.apachecon2014eu.demos.spdy;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		Dictionary<String, Object> props = new Hashtable<>();
		props.put("alias", "/spdy");

		context.registerService(Servlet.class.getName(), new DataServlet(), props);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Nop
	}
}
