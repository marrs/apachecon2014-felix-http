package net.luminis.apachecon2014eu.demos.rest;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		context.registerService(Object.class.getName(), new DemoRestEndpoint(), null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Nop
	}
}
