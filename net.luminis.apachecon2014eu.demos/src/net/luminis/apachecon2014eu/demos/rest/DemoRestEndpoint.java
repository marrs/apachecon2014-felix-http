package net.luminis.apachecon2014eu.demos.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;

@Path("/rest")
@Description("Provides a demo REST endpoint")
public class DemoRestEndpoint {
	private String m_response = "Hello World!";
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Description("Gives a plain text response")
	public String getPlainResponse() {
		return m_response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Description("Gives a JSON response")
	public String getJsonResponse() {
		return "{\"response\":\"" + m_response + "\"}";
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Description("Allows one to set the response to return")
	public void setResponse(@FormParam("response") @DefaultValue("Default response") String newResponse) {
		m_response = newResponse;
	}
}
