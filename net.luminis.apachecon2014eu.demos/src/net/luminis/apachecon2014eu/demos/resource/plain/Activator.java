package net.luminis.apachecon2014eu.demos.resource.plain;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
	private volatile HttpService m_httpService;
	
	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setImplementation(this)
			.add(createServiceDependency()
				.setService(HttpService.class)
				.setRequired(true)
			)
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {
	}
	
	public void start() throws Exception {
		m_httpService.registerResources("/site", "/resources", null);
	}
	
	public void stop() throws Exception {
		m_httpService.unregister("/site");
	}
}
