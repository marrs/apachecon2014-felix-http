This is an Eclipse workspace. After checking out the code, you can open the workspace in Eclipse. You need the Bndtools plugin for Eclipse, which can be found in the Eclipse Marketplace. More information about Bndtools can be found at http://bndtools.org/

To run the code, go to the net.luminis.apachecon2014eu.application project, click on the run.bndrun project and from the context menu choose: Run As / Bnd OSGi Run Launcher and point your browser to http://localhost:8080/ to see the slides.
