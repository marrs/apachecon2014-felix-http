/* slave control */
var EVENT_SLIDE_CHANGED = "esc", EVENT_FRAG_SHOWN = "efs", EVENT_FRAG_HIDDEN = "efh", EVENT_OVERVIEW_SHOWN = "eos", EVENT_OVERVIEW_HIDDEN = "eoh";
var wsConn;

// Open the connection to our server...
Reveal.addEventListener("ready", function(event) {
	var wsURL = (window.location.protocol == 'https:' ? 'wss://' : 'ws://') + window.location.host + window.location.pathname;
	wsURL = wsURL.replace(/\/?(index.html)?$/, '') + "/slidemgr";
    wsConn = new WebSocket(wsURL, [ "slides" ]);

    wsConn.onmessage = function(event) {
		var data = event.data.split(',');
		var name = data[0];
		switch (name) {
			case EVENT_SLIDE_CHANGED:
				Reveal.slide(data[1], data[2], data[3]);
				break;
			case EVENT_FRAG_SHOWN:
			case EVENT_FRAG_HIDDEN:
			case EVENT_OVERVIEW_SHOWN:
			case EVENT_OVERVIEW_HIDDEN:
				break;
		}
    }
} );

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}