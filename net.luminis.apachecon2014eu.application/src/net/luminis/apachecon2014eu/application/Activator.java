package net.luminis.apachecon2014eu.application;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		Dictionary<String, Object> props = new Hashtable<>();
		props.put("alias", "/slidemgr");
		context.registerService(Servlet.class.getName(), new SlideManager(), props);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Nop
	}
}
