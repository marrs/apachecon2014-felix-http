package net.luminis.apachecon2014eu.application;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class SlideManager extends WebSocketServlet {

	private static final long serialVersionUID = 1L;
	
	static final String PROTOCOL = "slides";
	static final String CONTROL_JS = "/control.js";
	static final String APPLICATION_JS = "application/javascript";
	
	static final int MASTER_TIMEOUT = -1; // no timeout
	static final int SLAVE_TIMEOUT = 15 * 60000; // 15 mins
	
	static final int MAX_CLIENTS = 500; // as if...

	private final Bundle m_bundle = FrameworkUtil.getBundle(getClass());
	private final List<SlideHandler> m_clients = new ArrayList<>();
	private final AtomicReference<SlideHandler> m_masterHandler = new AtomicReference<>();
	private volatile String m_lastEvent = null;

	@Override
	public void destroy() {
		try {
			List<SlideHandler> clients = new ArrayList<>();
			synchronized (m_clients) {
				clients.addAll(m_clients);
				m_clients.clear();
			}
			for (SlideHandler handler : clients) {
				handler.close();
			}
			
			m_masterHandler.set(null);
		} finally {
			super.destroy();
		}
	}

	@Override
	public WebSocket doWebSocketConnect(HttpServletRequest request, String protocol) {
		if (PROTOCOL.equalsIgnoreCase(protocol)) {
			int clientCount;
			synchronized (m_clients) {
				clientCount = m_clients.size();
			}
			if (clientCount < MAX_CLIENTS) {
				boolean master = isMaster(request);
				return new SlideHandler(master);
			}
		}

		return null;
	}

	@Override
	public void init() throws ServletException {
		try {
			synchronized (m_clients) {
				m_clients.clear();
			}

			m_masterHandler.set(null);
		} finally {
			super.init();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo();
		if (CONTROL_JS.equals(pathInfo)) {
			String fname = String.format("/reveal.%s.js", isMaster(req) ? "master" : "slave");

			URL control = m_bundle.getResource(fname);
			if (control != null) {
				streamResource(control, resp, APPLICATION_JS);
				return;
			}
		}

		super.doGet(req, resp);
	}

	private boolean isMaster(HttpServletRequest request) {
		try {
			InetAddress addr = InetAddress.getByName(request.getRemoteAddr());
			return addr.isLoopbackAddress();
		} catch (UnknownHostException e) {
			return false;
		}
	}

	private void streamResource(URL resource, HttpServletResponse response, String mimeType) throws ServletException, IOException {
		response.setContentType(mimeType);

		try (InputStream is = resource.openStream(); OutputStream os = response.getOutputStream()) {
			byte[] buffer = new byte[65535];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
		}
	}

	private class SlideHandler implements WebSocket.OnTextMessage {
		private final boolean m_master;
		private volatile Connection m_conn;
	
		public SlideHandler(boolean master) {
			m_master = master;
		}
	
		public void close() {
			if (m_conn != null) {
				m_conn.close();
				m_conn = null;
			}
		}
	
		@Override
		public void onClose(int code, String reason) {
			synchronized (m_clients) {
				m_clients.remove(this);
			}
	
			if (m_master) {
				// Master is shutting down, no need to keep its record around...
				m_masterHandler.set(null);
			} else {
				// Client shut down, inform the master about this...
				sendClientCount();
			}
		}
	
		@Override
		public void onMessage(String data) {
			if (data == null || "".equals(data.trim()) || !m_master) {
				return;
			}
	
			// We're the master...
			String lastEvent = m_lastEvent = data;
			synchronized (m_clients) {
				for (SlideHandler client : m_clients) {
					client.sendEvent(lastEvent);
				}
			}
		}
	
		@Override
		public void onOpen(Connection conn) {
			m_conn = conn;
			m_conn.setMaxIdleTime(m_master ? MASTER_TIMEOUT : SLAVE_TIMEOUT);
	
			if (!m_master) {
				// Send last known event...
				String lastEvent = m_lastEvent;
				sendEvent(lastEvent);
	
				synchronized (m_clients) {
					m_clients.add(this);
				}
			} else {
				// First one wins...
				if (!m_masterHandler.compareAndSet(null, this)) {
					System.err.println("Failed to update master?!");
				} else {
					// Ensure every other client is up-to-date...
					broadcastEvent("esc,0,0,0");
				}
			}
	
			sendClientCount();
		}
	
		private void broadcastEvent(String data) {
			List<SlideHandler> clients = new ArrayList<>();
			synchronized (m_clients) {
				clients.addAll(m_clients);
			}
			for (SlideHandler client : m_clients) {
				client.sendEvent(data);
			}
		}
	
		private void sendEvent(String data) {
			if (m_conn != null && m_conn.isOpen() && data != null && !"".equals(data.trim())) {
				try {
					m_conn.sendMessage(data);
				} catch (IOException e) {
					// We did our best...
					System.err.println("Failed to send message: " + e.getMessage());
				}
			}
		}
	
		private void sendClientCount() {
			SlideHandler master = m_masterHandler.get();
			if (master != null) {
				master.sendEvent("ecc," + m_clients.size());
			}
		}
	}
}
