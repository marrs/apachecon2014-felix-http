package net.luminis.http.spdy;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.http.jetty.ConnectorFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.spdy.api.SPDY;
import org.eclipse.jetty.spdy.http.HTTPSPDYServerConnector;
import org.eclipse.jetty.spdy.http.PushStrategy;
import org.eclipse.jetty.spdy.http.ReferrerPushStrategy;
import org.eclipse.jetty.util.ssl.SslContextFactory;

public class SpdyConnectorFactory implements ConnectorFactory {
	@Override
	public Connector createConnector() {
		SslContextFactory sslFactory = getSslContextFactory();
		Map<Short, PushStrategy> pushStrategies = getPushStrategies();

		HTTPSPDYServerConnector conn = new HTTPSPDYServerConnector(sslFactory, pushStrategies);
		conn.setConfidentialPort(8443);
		conn.setPort(8443);

		return conn;
	}

	/**
	 * @return a new {@link SslContextFactory} instance, configured with a sample keystore.
	 */
	private SslContextFactory getSslContextFactory() {
		SslContextFactory sslFactory = new SslContextFactory();
		sslFactory.setKeyStorePath("server.jks");
		sslFactory.setKeyStorePassword("secret");
		sslFactory.setProtocol("TLSv1");
		return sslFactory;
	}

	/**
	 * @return a map with the push strategy preferences for each SPDY version.
	 */
	private Map<Short, PushStrategy> getPushStrategies() {
		ReferrerPushStrategy strategy = new ReferrerPushStrategy();
		strategy.setReferrerPushPeriod(2500); // ms
		strategy.setMaxAssociatedResources(10);

		Map<Short, PushStrategy> pushStrategies = new HashMap<>(2);
		pushStrategies.put(SPDY.V2, strategy);
		pushStrategies.put(SPDY.V3, strategy);
		return pushStrategies;
	}
}
