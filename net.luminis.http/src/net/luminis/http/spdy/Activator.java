package net.luminis.http.spdy;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.http.jetty.ConnectorFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator
{
    /**
     * Qualified class name of the NPN SPI that should be available through the JVM bootclasspath.
     */
    private static final String NPN_NEXT_PROTO_NEGO = "org.eclipse.jetty.npn.NextProtoNego";

    public void start(BundleContext context) throws Exception
    {
        // Will throw an exception (an thus not register our service)...
        try
        {
            Class<?> npn = ClassLoader.getSystemClassLoader().loadClass(NPN_NEXT_PROTO_NEGO);
            if (npn.getClassLoader() != null)
            {
                throw new IllegalStateException("NextProtoNego must be on JVM boot path");
            }
        }
        catch (ClassNotFoundException e)
        {
            throw new IllegalStateException("No NextProtoNego on boot path", e);
        }

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("type", "spdy");

        context.registerService(ConnectorFactory.class.getName(), new SpdyConnectorFactory(), props);
    }

    public void stop(BundleContext context) throws Exception
    {
        // Nop
    }
}
